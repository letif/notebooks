# Notebooks

- [Annualization](./Annualization): Numerical experiments to demonstrate that annualization from daily return statistics is a proper approach only for `Sharpe ratio` and not for `CSharpe` and `Sortino` ratios. 
