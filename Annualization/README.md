# Investigating the consistency of annualization of widely used financial statistics

Numerical experiments to demonstrate that annualization from daily return statistics is a proper approach only for `Sharpe ratio` and not for `CSharpe` and `Sortino` ratios.
