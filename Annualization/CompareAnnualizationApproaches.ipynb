{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "17a4bfab",
   "metadata": {},
   "source": [
    "# Comparison of different annualization approaches"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "a578ed87",
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "\u001b[32m\u001b[1m  Activating\u001b[22m\u001b[39m project at `~/GitLab/notebooks/Annualization`\n"
     ]
    }
   ],
   "source": [
    "using Pkg\n",
    "Pkg.activate(\"./\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "6dddc228",
   "metadata": {},
   "outputs": [],
   "source": [
    "using CSV\n",
    "using DataFrames\n",
    "using Distributions\n",
    "using LinearAlgebra\n",
    "using Random\n",
    "using SpecialFunctions\n",
    "using Statistics\n",
    "using StatsBase"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "73c5fd40",
   "metadata": {},
   "source": [
    "## Define statistical functions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "9475ca85",
   "metadata": {},
   "outputs": [],
   "source": [
    "\"\"\"\n",
    "    Function to compute CVaR (aka expected shortfall) of returns with quantile parameter α (default value of 5%).\n",
    "\"\"\"\n",
    "function cvar(r::Vector{Float64}, α::Number=0.05)\n",
    "    @assert 0 < α < 1\n",
    "    K = Int(floor(length(r) * α)) # number of samples to consider at left tail\n",
    "    return -mean(partialsort(r, 1:K))\n",
    "end\n",
    "\n",
    "\"\"\"\n",
    "    Function to compute std of left tail (i.e. negative values) of returns by considering non-negative values as 0s.\n",
    "\"\"\"\n",
    "function std_left_zero(r::Vector{Float64})\n",
    "    return std(map(x -> x > 0 ? 0 : x, r))\n",
    "end\n",
    "\n",
    "\"\"\"\n",
    "    Function to compute std of left tail (i.e. negative values) of returns by skipping non-negative values.\n",
    "\"\"\"\n",
    "function std_left_skip(r::Vector{Float64})\n",
    "    return std(filter(x -> x < 0, r))\n",
    "end\n",
    "\n",
    "\"\"\"\n",
    "    Function to compute Sharpe ratio (i.e. mean / std) of returns.\n",
    "\"\"\"\n",
    "function Sharpe(r::Vector{Float64})\n",
    "    return mean(r) / std(r)\n",
    "end\n",
    "            \n",
    "\"\"\"\n",
    "    Function to compute CSharpe ratio (i.e. mean / cvar) of returns with quantile parameter α (default value of 5%).\n",
    "\"\"\"\n",
    "function CSharpe(r::Vector{Float64}, α::Number=0.05)\n",
    "    return mean(r) / cvar(r, α)\n",
    "end\n",
    "            \n",
    "\"\"\"\n",
    "Function to compute Sortino ratio (i.e. mean / std of negative side) of returns by setting non-negative values as 0s.\n",
    "\"\"\"\n",
    "function Sortino(r::Vector{Float64})\n",
    "    return mean(r) / std_left_zero(r)\n",
    "end\n",
    "            \n",
    "\"\"\"\n",
    "Function to compute Sortino ratio (i.e. mean / std of negative side) of returns by skipping non-negative values.\n",
    "\"\"\"\n",
    "function SortinoSkip(r::Vector{Float64})\n",
    "    return mean(r) / std_left_skip(r)\n",
    "end\n",
    "\n",
    "\"\"\"\n",
    "Function to compute C(α)\n",
    "\"\"\"\n",
    "C(α) = pdf(Normal(), quantile(Normal(), α)) / α\n",
    "\n",
    "\"\"\"\n",
    "Function to compute M(μ, σ)\n",
    "\"\"\"\n",
    "M(μ, σ) = μ / 2 * (erf(-μ / sqrt(2) / σ) + 1) - 1/sqrt(2π) * σ * exp(-μ^2/2/σ^2)\n",
    "\n",
    "\"\"\"\n",
    "Function to compute S(μ, σ)\n",
    "\"\"\"\n",
    "S(μ, σ) = 1/2 * (μ^2 + σ^2) * (erf(-μ / sqrt(2) / σ) + 1) - 1/sqrt(2π) * σ * μ * exp(-μ^2/2/σ^2)\n",
    "\n",
    "\"\"\"\n",
    "Function to compute N(μ, σ)\n",
    "\"\"\"\n",
    "N(μ, σ) = 1/2 * (erf(-μ / sqrt(2) / σ) + 1)\n",
    ";"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "0c5c1c16",
   "metadata": {},
   "outputs": [],
   "source": [
    "rng = MersenneTwister(1234)  # random seed for reproducibility\n",
    ";"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "3263aa9d",
   "metadata": {},
   "outputs": [],
   "source": [
    "r = CSV.read(\"./daily_returns.csv\", DataFrame)[!, 1]  # read daily returns\n",
    ";"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d97e4dbc",
   "metadata": {},
   "source": [
    "## Daily return statistics"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "ab2829a2",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div><div style = \"float: left;\"><span>9×2 DataFrame</span></div><div style = \"clear: both;\"></div></div><div class = \"data-frame\" style = \"overflow-x: scroll;\"><table class = \"data-frame\" style = \"margin-bottom: 6px;\"><thead><tr class = \"header\"><th class = \"rowNumber\" style = \"font-weight: bold; text-align: right;\">Row</th><th style = \"text-align: left;\">Statistics</th><th style = \"text-align: left;\">Value</th></tr><tr class = \"subheader headerLastRow\"><th class = \"rowNumber\" style = \"font-weight: bold; text-align: right;\"></th><th title = \"Symbol\" style = \"text-align: left;\">Symbol</th><th title = \"Float64\" style = \"text-align: left;\">Float64</th></tr></thead><tbody><tr><td class = \"rowNumber\" style = \"font-weight: bold; text-align: right;\">1</td><td style = \"text-align: left;\">mean</td><td style = \"text-align: right;\">2595.66</td></tr><tr><td class = \"rowNumber\" style = \"font-weight: bold; text-align: right;\">2</td><td style = \"text-align: left;\">std</td><td style = \"text-align: right;\">15397.0</td></tr><tr><td class = \"rowNumber\" style = \"font-weight: bold; text-align: right;\">3</td><td style = \"text-align: left;\">cvar</td><td style = \"text-align: right;\">24460.4</td></tr><tr><td class = \"rowNumber\" style = \"font-weight: bold; text-align: right;\">4</td><td style = \"text-align: left;\">std_left_zero</td><td style = \"text-align: right;\">6558.97</td></tr><tr><td class = \"rowNumber\" style = \"font-weight: bold; text-align: right;\">5</td><td style = \"text-align: left;\">std_left_skip</td><td style = \"text-align: right;\">9881.87</td></tr><tr><td class = \"rowNumber\" style = \"font-weight: bold; text-align: right;\">6</td><td style = \"text-align: left;\">Sharpe</td><td style = \"text-align: right;\">0.168582</td></tr><tr><td class = \"rowNumber\" style = \"font-weight: bold; text-align: right;\">7</td><td style = \"text-align: left;\">CSharpe</td><td style = \"text-align: right;\">0.106117</td></tr><tr><td class = \"rowNumber\" style = \"font-weight: bold; text-align: right;\">8</td><td style = \"text-align: left;\">Sortino</td><td style = \"text-align: right;\">0.395742</td></tr><tr><td class = \"rowNumber\" style = \"font-weight: bold; text-align: right;\">9</td><td style = \"text-align: left;\">SortinoSkip</td><td style = \"text-align: right;\">0.262669</td></tr></tbody></table></div>"
      ],
      "text/latex": [
       "\\begin{tabular}{r|cc}\n",
       "\t& Statistics & Value\\\\\n",
       "\t\\hline\n",
       "\t& Symbol & Float64\\\\\n",
       "\t\\hline\n",
       "\t1 & mean & 2595.66 \\\\\n",
       "\t2 & std & 15397.0 \\\\\n",
       "\t3 & cvar & 24460.4 \\\\\n",
       "\t4 & std\\_left\\_zero & 6558.97 \\\\\n",
       "\t5 & std\\_left\\_skip & 9881.87 \\\\\n",
       "\t6 & Sharpe & 0.168582 \\\\\n",
       "\t7 & CSharpe & 0.106117 \\\\\n",
       "\t8 & Sortino & 0.395742 \\\\\n",
       "\t9 & SortinoSkip & 0.262669 \\\\\n",
       "\\end{tabular}\n"
      ],
      "text/plain": [
       "\u001b[1m9×2 DataFrame\u001b[0m\n",
       "\u001b[1m Row \u001b[0m│\u001b[1m Statistics    \u001b[0m\u001b[1m Value        \u001b[0m\n",
       "     │\u001b[90m Symbol        \u001b[0m\u001b[90m Float64      \u001b[0m\n",
       "─────┼─────────────────────────────\n",
       "   1 │ mean            2595.66\n",
       "   2 │ std            15397.0\n",
       "   3 │ cvar           24460.4\n",
       "   4 │ std_left_zero   6558.97\n",
       "   5 │ std_left_skip   9881.87\n",
       "   6 │ Sharpe             0.168582\n",
       "   7 │ CSharpe            0.106117\n",
       "   8 │ Sortino            0.395742\n",
       "   9 │ SortinoSkip        0.262669"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df_r = DataFrame(:Statistics => Symbol[], :Value => Float64[])\n",
    "for stat_fun in [mean, std, cvar, std_left_zero, std_left_skip, Sharpe, CSharpe, Sortino, SortinoSkip]\n",
    "    push!(df_r, [Symbol(stat_fun), stat_fun(r)])\n",
    "end\n",
    "df_r"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "89f71ac1",
   "metadata": {},
   "source": [
    "## Annualized ratios"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "c367177a",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div><div style = \"float: left;\"><span>4×9 DataFrame</span></div><div style = \"clear: both;\"></div></div><div class = \"data-frame\" style = \"overflow-x: scroll;\"><table class = \"data-frame\" style = \"margin-bottom: 6px;\"><thead><tr class = \"header\"><th class = \"rowNumber\" style = \"font-weight: bold; text-align: right;\">Row</th><th style = \"text-align: left;\">Statistics</th><th style = \"text-align: left;\">Simple scaling</th><th style = \"text-align: left;\">Normal distribution approximation</th><th style = \"text-align: left;\">Subsample(10000)</th><th style = \"text-align: left;\">Subsample(100000)</th><th style = \"text-align: left;\">Subsample(1000000)</th><th style = \"text-align: left;\">Bootstrap(10000)</th><th style = \"text-align: left;\">Bootstrap(100000)</th><th style = \"text-align: left;\">Bootstrap(1000000)</th></tr><tr class = \"subheader headerLastRow\"><th class = \"rowNumber\" style = \"font-weight: bold; text-align: right;\"></th><th title = \"Function\" style = \"text-align: left;\">Function</th><th title = \"Float64\" style = \"text-align: left;\">Float64</th><th title = \"Float64\" style = \"text-align: left;\">Float64</th><th title = \"Float64\" style = \"text-align: left;\">Float64</th><th title = \"Float64\" style = \"text-align: left;\">Float64</th><th title = \"Float64\" style = \"text-align: left;\">Float64</th><th title = \"Float64\" style = \"text-align: left;\">Float64</th><th title = \"Float64\" style = \"text-align: left;\">Float64</th><th title = \"Float64\" style = \"text-align: left;\">Float64</th></tr></thead><tbody><tr><td class = \"rowNumber\" style = \"font-weight: bold; text-align: right;\">1</td><td style = \"text-align: left;\">Sharpe</td><td style = \"text-align: right;\">3.22076</td><td style = \"text-align: right;\">3.22076</td><td style = \"text-align: right;\">3.51327</td><td style = \"text-align: right;\">3.53418</td><td style = \"text-align: right;\">3.53185</td><td style = \"text-align: right;\">3.25874</td><td style = \"text-align: right;\">3.2065</td><td style = \"text-align: right;\">3.22462</td></tr><tr><td class = \"rowNumber\" style = \"font-weight: bold; text-align: right;\">2</td><td style = \"text-align: left;\">CSharpe</td><td style = \"text-align: right;\">2.02736</td><td style = \"text-align: right;\">-2.7812</td><td style = \"text-align: right;\">-2.26563</td><td style = \"text-align: right;\">-2.25094</td><td style = \"text-align: right;\">-2.24897</td><td style = \"text-align: right;\">-2.45694</td><td style = \"text-align: right;\">-2.51012</td><td style = \"text-align: right;\">-2.5045</td></tr><tr><td class = \"rowNumber\" style = \"font-weight: bold; text-align: right;\">3</td><td style = \"text-align: left;\">Sortino</td><td style = \"text-align: right;\">7.56063</td><td style = \"text-align: right;\">345.467</td><td style = \"text-align: right;\">30559.1</td><td style = \"text-align: right;\">3141.82</td><td style = \"text-align: right;\">2447.16</td><td style = \"text-align: right;\">2265.28</td><td style = \"text-align: right;\">878.606</td><td style = \"text-align: right;\">748.426</td></tr><tr><td class = \"rowNumber\" style = \"font-weight: bold; text-align: right;\">4</td><td style = \"text-align: left;\">SortinoSkip</td><td style = \"text-align: right;\">5.01828</td><td style = \"text-align: right;\">12.7262</td><td style = \"text-align: right;\">NaN</td><td style = \"text-align: right;\">20.6843</td><td style = \"text-align: right;\">26.4044</td><td style = \"text-align: right;\">86.5903</td><td style = \"text-align: right;\">17.5333</td><td style = \"text-align: right;\">14.5534</td></tr></tbody></table></div>"
      ],
      "text/latex": [
       "\\begin{tabular}{r|ccccc}\n",
       "\t& Statistics & Simple scaling & Normal distribution approximation & Subsample(10000) & \\\\\n",
       "\t\\hline\n",
       "\t& Function & Float64 & Float64 & Float64 & \\\\\n",
       "\t\\hline\n",
       "\t1 & Sharpe & 3.22076 & 3.22076 & 3.51327 & $\\dots$ \\\\\n",
       "\t2 & CSharpe & 2.02736 & -2.7812 & -2.26563 & $\\dots$ \\\\\n",
       "\t3 & Sortino & 7.56063 & 345.467 & 30559.1 & $\\dots$ \\\\\n",
       "\t4 & SortinoSkip & 5.01828 & 12.7262 & NaN & $\\dots$ \\\\\n",
       "\\end{tabular}\n"
      ],
      "text/plain": [
       "\u001b[1m4×9 DataFrame\u001b[0m\n",
       "\u001b[1m Row \u001b[0m│\u001b[1m Statistics  \u001b[0m\u001b[1m Simple scaling \u001b[0m\u001b[1m Normal distribution approximation \u001b[0m\u001b[1m Subsamp\u001b[0m ⋯\n",
       "     │\u001b[90m Function    \u001b[0m\u001b[90m Float64        \u001b[0m\u001b[90m Float64                           \u001b[0m\u001b[90m Float64\u001b[0m ⋯\n",
       "─────┼──────────────────────────────────────────────────────────────────────────\n",
       "   1 │ Sharpe              3.22076                            3.22076          ⋯\n",
       "   2 │ CSharpe             2.02736                           -2.7812\n",
       "   3 │ Sortino             7.56063                          345.467         30\n",
       "   4 │ SortinoSkip         5.01828                           12.7262\n",
       "\u001b[36m                                                               6 columns omitted\u001b[0m"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ratios = [Sharpe, CSharpe, Sortino, SortinoSkip]\n",
    "df_R = DataFrame(:Statistics => ratios)\n",
    "df_R[!, Symbol(\"Simple scaling\")] = map(ratio -> ratio(r) * sqrt(365), ratios)\n",
    "μ_R, σ_R = 365 * mean(r), sqrt(365) * std(r)\n",
    "df_R[!, Symbol(\"Normal distribution approximation\")] = [\n",
    "    μ_R / σ_R,\n",
    "    μ_R / (-μ_R + σ_R * C(0.05)),\n",
    "    μ_R / sqrt(S(μ_R, σ_R) - M(μ_R, σ_R)^2),\n",
    "    μ_R / sqrt(S(μ_R, σ_R)/N(μ_R, σ_R) - M(μ_R, σ_R)^2/N(μ_R, σ_R)^2)\n",
    "]\n",
    "for n in [10_000, 100_000, 1_000_000]\n",
    "    R = map(i -> sum(sample(rng, r, 365, replace=false)), 1:n)\n",
    "    df_R[!, Symbol(\"Subsample(\"*string(n)*\")\")] = map(ratio -> ratio(R), ratios)\n",
    "end\n",
    "for n in [10_000, 100_000, 1_000_000]\n",
    "    R = map(i -> sum(sample(rng, r, 365, replace=true)), 1:n)\n",
    "    df_R[!, Symbol(\"Bootstrap(\"*string(n)*\")\")] = map(ratio -> ratio(R), ratios)\n",
    "end\n",
    "df_R"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b8965066",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.8.5",
   "language": "julia",
   "name": "julia-1.8"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
